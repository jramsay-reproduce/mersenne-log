// Awful logging
function info(str) {
  if (process.env.LOG_LEVEL == "info" || process.env.LOG_LEVEL == "debug") {
    console.log("INFO: " + str);
  }
}

function debug(str) {
  if (process.env.LOG_LEVEL == "debug") {
    console.log("DEBUG: " + str);
  }
}

module.exports = {
  info,
  debug
};
